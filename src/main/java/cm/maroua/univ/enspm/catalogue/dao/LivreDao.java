package cm.maroua.univ.enspm.catalogue.dao;

import cm.maroua.univ.enspm.catalogue.entities.Auteur;
import cm.maroua.univ.enspm.catalogue.entities.Categorie;
import cm.maroua.univ.enspm.catalogue.entities.Livre;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author hounie
 */
@Repository
public interface LivreDao extends JpaRepository<Livre, Long>{
    public Page<Livre> findByTitreLike(String titre,Pageable p);
    public Page<Livre> findByAuteurNomLike(String name,Pageable p);
    public Page<Livre> findBySousCategorieNameLike(String name,Pageable p);
    
}
