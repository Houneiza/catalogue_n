/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.maroua.univ.enspm.catalogue.services;

import cm.maroua.univ.enspm.catalogue.services.impl.AuteurRessource;
import cm.maroua.univ.enspm.catalogue.services.impl.CategorieRessource;
import cm.maroua.univ.enspm.catalogue.services.impl.LivreRessource;
import cm.maroua.univ.enspm.catalogue.services.impl.SousCategorieRessource;
import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

/**
 *
 * @author hounie
 */
@Component
@ApplicationPath("/api")
public class ApplicationConfig extends ResourceConfig{
 
     public ApplicationConfig(){
        register(AuteurRessource.class);
        register(LivreRessource.class);
        register(SousCategorieRessource.class);
        register(CategorieRessource.class);
    }
     
}
