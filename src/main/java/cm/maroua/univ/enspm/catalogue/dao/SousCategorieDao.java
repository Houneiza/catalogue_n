/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.maroua.univ.enspm.catalogue.dao;

import cm.maroua.univ.enspm.catalogue.entities.SousCategorie;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author hounie
 */
public interface SousCategorieDao extends JpaRepository<SousCategorie, Long>{
    public List<SousCategorie> findBynameLike(String name);
}
