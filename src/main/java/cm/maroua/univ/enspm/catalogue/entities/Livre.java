package cm.maroua.univ.enspm.catalogue.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import lombok.Data;

/**
 *
 * @author hounie
 */
@Entity
@Data
public class Livre implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String titre;

    @Column(nullable = false)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date date_publication;

    @ManyToOne
    private SousCategorie sousCategorie;

    @ManyToOne
    private Auteur auteur;

}
